<?php

namespace Drupal\dynamic_filter_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormState;
use Drupal\views\Views;

/**
 * Provides a 'DynamicFilterBlock' block.
 *
 * @Block(
 *  id = "dynamic_filter_block",
 *  admin_label = @Translation("Dynamic Exposed Form"),
 *  category = "Views"
 * )
 */
class DynamicFilterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // We don't have much context during build, so we look at the current route
    // and try to decide if we're on a Views page.
    /** @var \Symfony\Component\HttpFoundation\ParameterBag $currentRouteParameters */
    $currentRouteParameters = \Drupal::routeMatch()->getParameters();

    $view_id = $currentRouteParameters->get('view_id', FALSE);
    $view_display_id = $currentRouteParameters->get('display_id', FALSE);

    // We don't want to build the block if this is not a Views page.
    if (!($view_id && $view_display_id)) {
      return [];
    }

    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView($view_id);
    $view->setDisplay($view_display_id);

    // We don't want to build the block if the view has no exposed filters.
    if (!$view->display_handler->usesExposed()) {
      return [];
    }

    // Construct the exposed form.
    $view->initHandlers();
    /** @var \Drupal\views\Plugin\views\exposed_form\ExposedFormPluginBase $exposed_form_plugin */
    $exposed_form_plugin = $view->display_handler->getPlugin('exposed_form');

    $form_state = new FormState();
    $form_state->setFormState([
      'view' => $view,
      'display' => $view->display_handler->display,
      'exposed_form_plugin' => $exposed_form_plugin,
      'method' => 'get',
      'rerender' => TRUE,
      'no_redirect' => TRUE,
      'always_process' => TRUE,
    ]);

    // Construct our block.
    $build = [];
    $build['#theme'] = 'dynamic_filter_block';
    $build['#cache'] = [
      // Cache our block per page, rather than the per block default.
      'contexts' => ['route.name'],
    ];
    $build['exposed_form'] = \Drupal::formBuilder()->buildForm('Drupal\views\Form\ViewsExposedForm', $form_state);

    return $build;
  }

}
